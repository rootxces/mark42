package com.example.jack_sparrow.mark42;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.jack_sparrow.mark42.dummy.DummyContent;
import com.example.jack_sparrow.mark42.fragments.ClassListFragment;
import com.example.jack_sparrow.mark42.fragments.FragmentStudent;
import com.example.jack_sparrow.mark42.fragments.PreBeginAttendance;

public class AttendanceActivity extends AppCompatActivity implements ClassListFragment.OnListFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.attendanceRootView, new ClassListFragment());
        ft.commit();

    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.attendanceRootView, new PreBeginAttendance());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }



}
