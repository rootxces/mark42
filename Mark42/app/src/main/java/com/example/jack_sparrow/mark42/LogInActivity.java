package com.example.jack_sparrow.mark42;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LogInActivity extends AppCompatActivity {

    public static final String MY_DB = "my_db";

    EditText username;
    EditText password;
    Button logInButton;
    public SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = getSharedPreferences(MY_DB, Context.MODE_PRIVATE);

        if(sp.getBoolean("isLogin",false) == false){
            setContentView(R.layout.activity_log_in);

            logInButton = (Button) findViewById(R.id.logInButton);
            logInButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SharedPreferences.Editor e = sp.edit();
                    e.putBoolean("isLogin", true);
                    e.commit();
                    Intent intent = new Intent(LogInActivity.this, MainActivityFaculty.class);
                    startActivity(intent);
                    finish();
                }
            });

        }

        else{
            Intent intent = new Intent(LogInActivity.this, MainActivityFaculty.class);
            startActivity(intent);
            finish();
        }




    }
}
