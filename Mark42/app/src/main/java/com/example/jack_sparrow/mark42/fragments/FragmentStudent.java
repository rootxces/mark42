package com.example.jack_sparrow.mark42.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jack_sparrow.mark42.R;
import com.example.jack_sparrow.mark42.testing.purpose.code.StudentListTest;

import static com.example.jack_sparrow.mark42.testing.purpose.code.StudentListTest.students;

/**
 * Created by jack-sparrow on 25/1/18.
 */

public class FragmentStudent extends Fragment {

    int unNecessaryFlag = 0;

    TextView studentNameTextView;
    TextView studentRollNoTextView;
    TextView studentAttendancePercentTextView;
    Button presentButton;
    Button absentButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_student,null);
    }

    @Override
    public void onViewCreated(final View viewr, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(viewr, savedInstanceState);

        beginAttendance(viewr, unNecessaryFlag);

        presentButton = (Button) viewr.findViewById(R.id.presentButton);
        absentButton = (Button) viewr.findViewById(R.id.absentButton);


        presentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unNecessaryFlag++;
//                Toast.makeText(getActivity(), "This button has no functionality yet", Toast.LENGTH_SHORT).show();
                beginAttendance(viewr,unNecessaryFlag);
            }
        });

        absentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unNecessaryFlag--;
//                Toast.makeText(getActivity(), "This button has no functionality yet", Toast.LENGTH_SHORT).show();
                beginAttendance(viewr, unNecessaryFlag);
            }
        });

    }

    public void beginAttendance(View view, int flag){
        if(flag<=9 && flag>=0){
            studentNameTextView = (TextView) view.findViewById(R.id.studentName);
            studentRollNoTextView = (TextView) view.findViewById(R.id.studentRollNo);
            studentAttendancePercentTextView = (TextView) view.findViewById(R.id.studentAttendancePercent);

            studentNameTextView.setText(students[flag].studentName);
            studentRollNoTextView.setText(students[flag].studentRollNo);
            studentAttendancePercentTextView.setText(students[flag].studentAttendance);
        }
        else
        {
            Toast.makeText(getActivity(), "End of Students!",Toast.LENGTH_SHORT).show();
        }

    }
}
