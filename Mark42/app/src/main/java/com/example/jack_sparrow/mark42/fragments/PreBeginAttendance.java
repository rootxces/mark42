package com.example.jack_sparrow.mark42.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.jack_sparrow.mark42.R;

/**
 * Created by jack-sparrow on 31/1/18.
 */

public class PreBeginAttendance extends Fragment {

    private Button begin;
    private Button review;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pre_begin_attendance, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        begin = (Button) view.findViewById(R.id.beginAttendanceButton);
        review = (Button) view.findViewById(R.id.reviewAttendance);

        begin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.attendanceRootView, new FragmentStudent());
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.attendanceRootView, new ReveiwAttendance());
                ft.addToBackStack(null);
                ft.commit();
            }
        });


    }

}
