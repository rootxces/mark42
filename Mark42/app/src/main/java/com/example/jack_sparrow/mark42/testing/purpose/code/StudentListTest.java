package com.example.jack_sparrow.mark42.testing.purpose.code;

import com.example.jack_sparrow.mark42.classes.Student;

/**
 * Created by jack-sparrow on 25/1/18.
 */

public class StudentListTest{
    public static Student[] students = new Student[10];
    public void fillStudents(){
        for(int i =0; i<students.length; i++){
            students[i] = new Student();
            students[i].studentName = "Name "+i;
            students[i].studentRollNo = "RollNo "+i;
            students[i].studentAttendance = "Attendance "+i;
        }
    }
}
